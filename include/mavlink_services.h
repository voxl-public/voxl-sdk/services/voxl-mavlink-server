/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef MAVLINK_SERVICES_H
#define MAVLINK_SERVICES_H

#include <c_library_v2/common/mavlink.h>


// when using an external autopilot over UART, we respond to timesync messages
// so the autopilot can estimate our time offset. This is very important since
// VIO data from VOXL is sent with linux clock_monotonic timestamps
// Note this is not relevant for PX4 running on SLPI since in that case
// the autopilot and VOXL share the same system clock
void mavlink_services_handle_timesync(mavlink_message_t* msg);
void mavlink_services_en_debug_timesync(void);
int64_t mavlink_services_get_ap_ns_ahead_of_voxl(void);

// The autopilot has the benefit of low-level access to the GPS and can
// therefore set its system time by GPS. We can subscribe to the system time
// message and set the VOXL linux clock_realtime from this when internet access
// is missing. TODO validate this on VOXL2, working on VOXL1.
void mavlink_services_handle_system_time(mavlink_message_t* msg);



#endif // end #define MAVLINK_SERVICES_H
