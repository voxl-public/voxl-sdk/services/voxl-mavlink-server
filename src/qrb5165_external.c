/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>
#include <pthread.h>
#include <unistd.h>	// for  close
#include <errno.h>
#include <poll.h>
#include <modal_pipe_server.h>
#include <voxl_io/uart.h>


#include "qrb5165_external.h"
#include "config_file.h"
#include "pipe_io.h"
#include "gcs_io.h"
#include "common.h"

#define READ_BUF_LEN (2*1024)
#define MAV_CHAN	0



static int running;
static pthread_t recv_thread_id_local;
static int print_debug_send;
static int print_debug_recv;


void qrb5165_external_io_en_print_debug_send(int en_print_debug)
{
	printf("Enabling qrb5165 external AP send debugging\n");
	print_debug_send = en_print_debug;
	return;
}

void qrb5165_external_io_en_print_debug_recv(int en_print_debug)
{
	printf("Enabling qrb5165 external AP recv debugging\n");
	print_debug_recv = en_print_debug;
	return;
}

int qrb5165_external_io_send_any_msg(mavlink_message_t* msg)
{
	// nothing to do if shutting down
	if(!running) return -1;

	// basic debugging help
	if(print_debug_send){
		printf("to AP msg ID: %5d sysid:%3d   to bus: %d\n", msg->msgid, msg->sysid, autopilot_uart_bus);
	}

	// unpack message into a buffer ready to send
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];
	size_t bytes = mavlink_msg_to_send_buffer(buf, msg);


	int bytes_written = voxl_uart_write(autopilot_uart_bus, buf, bytes);

	if (bytes_written < 0) {
		fprintf(stderr, "Error, voxl_uart_write returned error code %d\n", bytes_written);
		return -1;
	}
	if ((size_t) bytes_written < bytes) {
		fprintf(stderr, "Error, UART write incomplete\n");
		return -1;
	}

	//printf("%5d %5d %5d, %5d\n",msg->msgid, bytes, bytes_written, MAVLINK_MAX_PACKET_LEN);

	return 0;
}



// with external FC no choice but to send all messages into the same port
int qrb5165_external_io_send_onboard_msg(mavlink_message_t* msg)
{
	return qrb5165_external_io_send_any_msg(msg);
}

int qrb5165_external_io_send_gcs_msg(mavlink_message_t* msg)
{
	return qrb5165_external_io_send_any_msg(msg);
}


// thread to read incoming mavlink packets
static void* _recv_local_thread_func(__attribute__((unused)) void *vargp)
{
	printf("starting qrb5165 external AP receive thread\n");

	// general local variables
	int i, bytes_read;
	mavlink_message_t msg;
	mavlink_status_t status;
	int msg_received = 0;
	uint8_t buf[READ_BUF_LEN];

	memset(&msg, 0, sizeof(msg));
	memset(&status, 0, sizeof(status));

	// keep going until qrb5165_external_io_stop sets running to 0
	while (running) {
		bytes_read = voxl_uart_read_bytes(autopilot_uart_bus, buf, READ_BUF_LEN);
		if (bytes_read < 0) {
			fprintf(stderr, "ERROR: Got negative return code from voxl_uart_read_bytes on bus %d\n",
					autopilot_uart_bus);
			continue;
		}

		// do the mavlink byte-by-byte parsing
		for(i = 0; i < bytes_read; i++) {
			msg_received = mavlink_frame_char(MAV_CHAN_AP_OBD, buf[i], &msg, &status);

			// check for dropped packets
			if (status.packet_rx_drop_count != 0) {
				fprintf(stderr,"WARNING: UART parser dropped %d packets from external AP at i=%d\n", status.packet_rx_drop_count, i);
			}

			// msg_received indicates this byte was the end of a complete packet
			if (msg_received){
				if (print_debug_recv) {
					printf("RECV  msg ID: %5d sysid:%3d from bus: %d\n", msg.msgid, msg.sysid, autopilot_uart_bus);
				}
				// send ALL message out to the pipe
				// even those with CRC/signature errors since the error is likely not an
				// integrity issue, just a mismatch in version, or the user has their own
				// custom mavlink dialect that the receiving end can decode without error.
				pipe_io_publish_to_both_channels(&msg);

				// also send everything to GCS
				gcs_io_send_to_gcs(&msg);
			}
		}
	}

	printf("exiting read thread\n");
	return NULL;
}


int qrb5165_external_io_init(void)
{
	int rval = voxl_uart_init(autopilot_uart_bus, autopilot_uart_baudrate, 0.1f, false, 1, false);
	if (rval < 0) {
		fprintf(stderr, "ERROR: Unable to configure bus %d (baudrate %d) with voxl_io\n", autopilot_uart_bus, autopilot_uart_baudrate);
		return -1;
	} else {
		printf("Successfully opened bus %d at baudrate %d\n", autopilot_uart_bus, autopilot_uart_baudrate);
	}


	// flag to the send function and receive thread that the socket is configured
	running = 1;

	// start the receiving thread
	pthread_create(&recv_thread_id_local, NULL, _recv_local_thread_func, NULL);

	// let the thread start
	usleep(100000);

	return 0;
}


int qrb5165_external_io_stop(void)
{
	if(running==0){
		return 0;
	}

	running=0;
	pthread_join(recv_thread_id_local, NULL);
	int rval = voxl_uart_close(autopilot_uart_bus);
	if (rval == -1) {
		fprintf(stderr, "ERROR: Received bad return code %d from voxl_uart_close on bus %d\n", rval, autopilot_uart_bus);
	}

	printf("qrb5165 external ap interface stopped\n");
	return 0;
}
