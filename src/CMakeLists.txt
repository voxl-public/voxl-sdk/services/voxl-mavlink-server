cmake_minimum_required(VERSION 3.3)

set(TARGET voxl-mavlink-server)

if(PLATFORM MATCHES APQ8096)
	message(STATUS "Building for platform APQ8096")
	add_executable(${TARGET}
		main.c
		common.c
		mavlink_services.c
		config_file.c
		pipe_io.c
		gcs_io.c
		apq8096_interface.c
	)
	add_definitions(-DPLATFORM_APQ8096)
	target_link_libraries(${TARGET}
		pthread
		modal_json
		modal_pipe
		voxl_io

	)
else()
	message(STATUS "Building for platform QRB5165")
	add_executable(${TARGET}
		main.c
		common.c
		mavlink_services.c
		config_file.c
		pipe_io.c
		gcs_io.c
		qrb5165_interface.c
		qrb5165_external.c
		qrb5165_slpi.c
	)
	add_definitions(-DPLATFORM_QRB5165)
	target_include_directories(${TARGET} PRIVATE /usr/include/voxl_io)

	target_link_libraries(${TARGET}
		pthread
		/usr/lib64/libmodal_json.so
		/usr/lib64/libmodal_pipe.so
			voxl_io
	)
endif()

include_directories(../include/)




install(
	TARGETS ${TARGET}
	LIBRARY         DESTINATION /usr/lib
	RUNTIME         DESTINATION /usr/bin
	PUBLIC_HEADER   DESTINATION /usr/include
)
