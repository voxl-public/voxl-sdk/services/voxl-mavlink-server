/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 ******************************************************************************/


#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>


// this is the directory used by the voxl-hello-server for named pipes
#define PIPE_NAME	"gcs_ip_list"

// you may need a larger buffer for your application!
#define PIPE_READ_BUF_SIZE	1024
#define CLIENT_NAME	"voxl-inspect-gcs-ip"
#define CH 0

#define CLEAR_LINE			"\033[2K"	// erases line but leaves curser in place

static void _print_usage(void)
{
	printf("\n\
This is a tool to print a list of currently connected ground control stations.\n\
The data is published by voxl-mavlink-server at gcs_ip_list\n\
\n\
-h, --help                  print this help message\n\
\n");
	return;
}

// called whenever the simple helper has data for us to process
static void _simple_cb(__attribute__((unused)) int ch, char* data, int bytes, \
										__attribute__((unused)) void* context)
{
	printf(CLEAR_LINE);

	if(bytes<=0){
		printf("\rnone");
	}
	else{
		printf("\r%s", data);
	}
	fflush(stdout);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, CLEAR_LINE "\rdisconnected from server");
	fflush(stdout);

	return;
}


// not many command line arguments
static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{0, 0, 0, 0}
	};
	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "h", long_options, &option_index);
		if(c == -1) break; // Detect the end of the options.
		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			return -1;
		default:
			_print_usage();
			return -1;
		}
	}
	return 0;
}


int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	int flags 						= CLIENT_FLAG_EN_SIMPLE_HELPER;


	printf("waiting for server");


	// assign callabcks for data, connection, and disconnect. the "NULL" arg
	// here can be an optional void* context pointer passed back to the callbacks
	pipe_client_set_simple_helper_cb(CH, _simple_cb, NULL);
	pipe_client_set_disconnect_cb(CH, _disconnect_cb, NULL);
	pipe_client_open(CH, PIPE_NAME, CLIENT_NAME, flags, PIPE_READ_BUF_SIZE);

	// keep going until signal handler sets the main_running flag to 0
	while(main_running){
		usleep(200000);
	}

	// all done, signal pipe read threads to stop
	printf("closing\n");
	fflush(stdout);
	pipe_client_close_all();

	return 0;
}
